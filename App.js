/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import Main from './src/Components/Main';
import { Provider } from 'react-redux';
import {KnongdaiCenstralStore} from './src/Stores/KnongdaiCenstralStore'
export default class App extends Component {
  render() {
    return (
      <Provider store={KnongdaiCenstralStore}>
        <Main />
      </Provider>
    );
  }
}


