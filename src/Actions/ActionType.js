export const ActionType={
    KNONGDAI_FETCH_MAIN_CATEGORY:'KNONGDAI_FETCH_MAIN_CATEGORY',
    KNONGDAI_FETCH_SUB_CATEGORY:'KNONGDAI_FETCH_SUB_CATEGORY',
    KNONGDAI_FETCH_KEYWORD_RESULT:'KNONGDAI_FETCH_KEYWORD_RESULT',
    KNONGDAI_FETCH_KEYWORD_SUGGESTION:'KNONGDAI_FETCH_KEYWORD_SUGGESTION',
    KNONGDAI_FETCH_POPULAR_URL:'KNONGDAI_FETCH_POPULAR_URL',
    KNONGDAI_POST_URL_REQUEST:'KNONGDAI_POST_URL_REQUEST',
    KNONGDAI_CREATE_USER:'KNONGDAI_CREATE_USER',
    KNONGDAI_FETCH_USER_BY_FACEBOOKID:'KNONGDAI_FETCH_USER_BY_FACEBOOKID',
    // BASE_RUL:'http://www.knongdai.com',
    // BASE_RUL:'http://localhost:9090',
    BASE_RUL:'http://35.240.224.58:28080',
    
    
}