import React, { Component } from 'react';
import {View,Text,Image,Platform,StyleSheet,Animated,Dimensions} from 'react-native'
import {Actions} from 'react-native-router-flux'

var {height,width} = Dimensions.get('window')
class Splash extends Component {
    static navigationOptions = {
        header : null,
    }
    state={
        animateOpcity: new Animated.Value(0),
        titleMarginTop: new Animated.Value(height/3)
    }

    async componentDidMount(){
        //Add animations here
        Animated.sequence([
            //animations by sequence
            Animated.timing(this.state.animateOpcity,{
                toValue:1,
                duration:1700
            }),
            //animate text
            Animated.timing(this.state.titleMarginTop,{
                toValue:10,
                duration:1000
            })

        ]).start(()=>{
            //navigate to next screen here
            Actions.home()
        })
    }

    render() {
        return (
            <View style={styles.container}>
               <Animated.Image
                source={require('../Image/logo-KD-small.png')}
                style={[styles.logoStyle,{opacity:this.state.animateOpcity}]}
              / >
                <Animated.Text style={[styles.txtStyle,{marginTop:this.state.titleMarginTop}]}>
                    ពត័មានគ្រប់យ៉ាងនៅក្នុងដៃអ្នក
                </Animated.Text>
                <View style={styles.txtBottomContainer}>
                    <Text style={[styles.txtBottom]}>
                        www.knongdai.com
                    </Text>
                </View>
            </View>
        );
    }
}

export default Splash;

const styles=StyleSheet.create({
    container:{
        paddingTop:Platform.OS==='ios'? 40:0 ,
        flex:1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'white',
    },
    logoStyle:{
        width:100,height:100,
    },
    txtStyle:{
        fontSize:16,
        fontWeight: 'bold',
        color:'rgb(233, 150, 63)'
    },
    txtBottom:{
        fontSize:12,
        textAlign:'center',
        color:'#444444'
    },
    txtBottomContainer:{
        position:'absolute',flex:1,
        bottom:Platform.OS==='android'? 5:20
    }
})