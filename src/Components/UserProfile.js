import React, { Component } from 'react';
import { View, Text, StyleSheet, Image,AsyncStorage } from 'react-native';
import { Container, Tabs, Tab, Card, CardItem, Icon, Right, Thumbnail, Button, Header, Content } from 'native-base'
import ImageLogo from './imageLogo';
import { fetchUserByFacebookId } from '../Actions/KnongDaiAction';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            facebookId:""
        };
    }
    getUser = async () => {
        try {
            const facebookId = await AsyncStorage.getItem('facebookId');
            // console.log(facebookId)
            console.log('before  update =',this.state.facebookId)
            this.setState({facebookId})
            console.log('after update = ',this.state.facebookId)
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }

    componentWillMount() {
        this.getUser()
        // 1065055700341401
    }
    componentDidMount=()=>{
        // this.getUser()
        const facebookId = "";
         AsyncStorage.getItem('facebookId')
         .then(res=> this.props.fetchUserByFacebookId(res))
       // console.log("State Facebook ID : ",facebookId)
      
    }

    render() {
        const {knongdaiUser}=this.props
        console.log('KnongdaiUser=====>',knongdaiUser)
        return (
            <Container>
                <Content>
                    <ImageLogo />
                    <Tabs >
                        <Tab heading="Personal Information">
                            <Card style={Object.assign({}, styles.card, styles.title)}>
                                <Text style={styles.itemtitle}>Name</Text>
                                <Text style={styles.item}>{knongdaiUser.userName}</Text>
                                <Text style={styles.itemtitle}>Gender</Text>
                                <Text style={styles.item}>{knongdaiUser.gender}</Text>
                                <Text style={styles.itemtitle}>Phone</Text>
                                <Text style={Object.assign({}, styles.item, styles.buttom)}>{knongdaiUser.phoneNumber}</Text>

                            </Card>
                            <Button
                                style={{ alignSelf: 'center', width: 200, alignItems: 'center', justifyContent: 'center', backgroundColor: '#F7941D' }}
                                onPress={() => Actions.edituser()}
                            >
                                <Text style={{ alignSelf: 'center', color: '#FFFFFF' }}>Edit</Text>
                            </Button>
                        </Tab>
                        <Tab heading="History">
                            <Card style={styles.card}>
                                <CardItem>
                                    <Text style={{ color: '#3E7F83' }}>School</Text>
                                    <Right style={styles.rigth}>
                                        <Icon name="paper-plane" style={{ color: '#3E7F83' }} />
                                    </Right>
                                </CardItem>
                                <CardItem>
                                    <Text style={{ color: '#3E7F83' }}>Hospital</Text>
                                    <Right style={styles.rigth}>
                                        <Icon name="paper-plane" style={{ color: '#3E7F83' }} />
                                    </Right>
                                </CardItem>
                            </Card>
                        </Tab>
                    </Tabs>
                </Content>
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        knongdaiUser: state.knongdais.knongdaiUser,
    }
}
export default connect(mapStateToProps, { fetchUserByFacebookId })(UserProfile)
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Roboto'
    },
    title: {
        fontSize: 32,
        color: '#F7941D',
        marginBottom: 10,
    },
    card: {
        marginTop: 5,
        borderColor: '#F7941D',
        borderWidth: 10,
    },
    rigth: {
        marginRight: -60,
    },
    itemtitle: {
        marginLeft: 15,
        fontSize: 16,
        marginTop: 10,
    },
    item: {
        fontSize: 18,
        marginLeft: 30,
        marginTop: 5,
    },
    buttom: {
        marginBottom: 10,
    },
    cardPhoto: {
        marginTop: 10,
        borderColor: '#F7941D',
        borderWidth: 10,
        marginLeft: 20,
        marginRight: 20,
        height: 150,
        alignItems: 'center',
        justifyContent: 'center'
    },
})
