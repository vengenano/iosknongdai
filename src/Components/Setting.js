import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch,Button ,Title,} from 'native-base';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import {StyleSheet,Image} from 'react-native';
import {Actions} from 'react-native-router-flux'
export default class Setting extends Component {
  render() {
    return (
      <Container>
        <Content>
            <List>
                <ListItem>
                    <Body>
                        <Text style={{color:'black',fontSize:18}}>Langauge</Text>
                        <Text note style={{fontSize:11}}>
                            By default is Khmer
                        </Text>
                    </Body>
                
                    <Right>
                        <RadioGroup
                            style={styles.ratio}>     
                                <RadioButton value={'Khmer'} >
                                <Image source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Flag_of_Cambodia.svg/510px-Flag_of_Cambodia.svg.png'}} style={{width: 30, height: 20}} />
                                </RadioButton>
                                    
                                <RadioButton value={'English'}  >
                                <Image source={{uri: 'https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/510px-Flag_of_the_United_Kingdom.svg.png'}} style={{width: 30, height: 20}} />
                                </RadioButton>                 
                        </RadioGroup>
                    </Right>
                </ListItem>
                <ListItem>
                    <Body >
                        <Text style={{color:'black',fontSize:18}}>Notication</Text>
                        <Text note style={{fontSize:11,}}>
                                Allows KNONG DAI to send notications
                        </Text>
                    </Body>
                    <Right>
                        <Switch value={true} />
                    </Right>
                </ListItem>
                <ListItem
                    onPress={()=>Actions.policy()}
                >       
                    <Body>
                    <Text>Policy</Text>
                    </Body>
                    <Right>
                    <Icon active name="arrow-forward" />
                    </Right>        
                </ListItem>
                <ListItem
                onPress={()=>Actions.about()}
                >
                    <Body >
                        <Text>About Us</Text>
                    </Body>
                    <Right>
                        <Icon active name="arrow-forward" />
                    </Right>
                </ListItem>
            </List>
        </Content>
      </Container>
    );
  }
}
const styles=StyleSheet.create({
  ratio:{
    flex:1,
    flexDirection: 'row',
    marginRight:20,

  },
  itemHeight:{
      paddingTop:10,
      paddingBottom: 10,
  }

})