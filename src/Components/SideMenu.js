import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Text, View, StyleSheet, Platform, Image,AsyncStorage } from 'react-native'
import { Content, List, ListItem } from 'native-base';
import Axios from 'axios';
import RNAccountKit from 'react-native-facebook-account-kit'
import { Actions } from 'react-native-router-flux'
import { fetchUserByFacebookId } from '../Actions/KnongDaiAction';
import { connect } from 'react-redux';

class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
        facebookId:"",
    };
  }

  sendRequestForPhoneNumber = async (token) => {

    try {
      // console.log('token from system: ', token);
      var access_token_data = ['AA', '304340463524448', '9010ea6a6b5cbd6e36d16ed0da0cb698']

      var access_token = access_token_data.join('|');

      var token_exchange_url = `https://graph.accountkit.com/v1.1/access_token?grant_type=authorization_code&code=${token}&access_token=${access_token}`;
      let getTokens = await Axios(token_exchange_url);
      let getDetailsUrl = `https://graph.accountkit.com/v1.1/me?access_token=${getTokens.data.access_token}`;

      let getDetailsUser = await Axios(getDetailsUrl);
      return getDetailsUser.data;
    } catch (error) {
      console.log('from the catch block: ', error);
    }
  }
  handleOnLogin() {
    RNAccountKit.configure({
      responseType: 'code', // 'token' by default,
      titleType: 'login',
      // initialAuthState: '',
      // initialEmail: 'some.initial@email.com',
      initialPhoneCountryPrefix: '+855', // autodetected if none is provided
      // initialPhoneNumber: '123-456-7890',
      // facebookNotificationsEnabled: true|false, // true by default
      // readPhoneStateEnabled: true|false, // true by default,
      receiveSMS: true, // true by default,
      // countryWhitelist: ['AR'], // [] by default
      // countryBlacklist: ['US'], // [] by default
      defaultCountry: 'KM',
      // theme: {...}, // for iOS only, see the Theme section
      // viewControllerMode: 'show'|'present' // for iOS only, 'present' by default
      getACallEnabled: true
    })
    RNAccountKit.loginWithPhone()
      .then(async (token) => {
        if (!token) {
          console.log('Login cancelled')
        } else {
          console.log(`Log with phone ${JSON.stringify(token)} `)
          var getResults = await this.sendRequestForPhoneNumber(token.code);
          console.log("Here is Result : ", getResults)
          const param = {
            facebookId: getResults.id,
            phoneNumber: getResults.phone.number
          }
          Actions.register(param)
        }
      })

  }
  handleOnUserProfile() {
    
    Actions.userprofile()
  }
  
  getUser = async () => {
    try {
        const facebookId = await AsyncStorage.getItem('facebookId');
        this.setState({facebookId})
    } catch (error) {
        console.log("Error retrieving data" + error);
    }
  }

  componentWillMount() {
      this.getUser()
  }
  componentDidMount=()=>{

      const facebookId = "";
      AsyncStorage.getItem('facebookId')
      .then(res=> this.props.fetchUserByFacebookId(res))
      
  }

  render() {
    const {knongdaiUser}=this.props
    console.log('=============>>>>>>>>>>',this.props)
    console.log('=================fb=========',this.state.fbObj)
    return (
      <View style={styles.container}>
        <View
          style={styles.boxHeader}>
          <View style={styles.imgWrapper}>
            <Image source={require('../Image/kd-circle1.png')}
              style={styles.imgSize}
            />
          </View>
          <View style={styles.boxName}>
            <Text style={styles.name}> {knongdaiUser? knongdaiUser.userName :"មិនទាន់មានទិន្នន័យអ្នកប្រើប្រាស់"}</Text>
            <Text style={styles.email}>{knongdaiUser? knongdaiUser.phoneNumber : " "}</Text>
          </View>
        </View>
        <View style={styles.menuWrapper}>
          <Content>
            <List>
              <ListItem
                onPress={() => { Actions.drawerClose(); Actions.home(); }}
              >
                <Image
                  source={require('../Image/icons8-home-filled-100.png')}
                  style={styles.menuIcon}
                />
                <Text
                  style={styles.menuText}
                >HOME
                </Text>
              </ListItem>
              <ListItem
                onPress={() =>  {knongdaiUser?  this.handleOnUserProfile() : Actions.handleOnLogin()} }
              >
                <Image
                source={require('../Image/icons8-user-100.png')}
                style={styles.menuIcon}
              />
              <Text style={styles.menuText}>USER PROFILE</Text>
              </ListItem>
              
            <ListItem
              onPress={() => {knongdaiUser? Actions.urlrequest() : Actions.handleOnLogin()}}
            >
              <Image
                source={require('../Image/icons8-geography-filled-100.png')}
                style={styles.menuIcon}
              />
              <Text style={styles.menuText}
              >REQUEST WEBSITE</Text>
            </ListItem>
            <ListItem
              onPress={() => Actions.setting()}
            >
              <Image
                source={require('../Image/icons8-services-480.png')}
                style={styles.menuIcon}
              />
              <Text
                style={styles.menuText}
              >SETTING
                          </Text>

            </ListItem>
            {knongdaiUser ?
              <ListItem
              onPress={() => alert('logout')}
              >
                <Image
                  source={require('../Image/icons8-sign-out-100.png')}
                  style={styles.menuIcon}
                />
                <Text style={styles.menuText} >LOG OUT</Text>
              </ListItem>
              :
              <ListItem
                onPress={() =>this.handleOnLogin()}
              >
                <Image
                  source={require('../Image/icons8-login-100.png')}
                  style={styles.menuIcon}
                />
                <Text style={styles.menuText} >LOG IN</Text>
              </ListItem>
            }
            </List>
          </Content>
      </View>
      </View >
    )
  }


}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

const mapStateToProps = (state) => {
  return {
      knongdaiUser: state.knongdais.knongdaiUser,
  }
}

export default connect(mapStateToProps, { fetchUserByFacebookId })(SideMenu)

const styles = StyleSheet.create({
  container: {
    flex: 1, paddingTop: Platform.OS == 'ios' ? 30 : 0, backgroundColor: '#F7941D'
  }
  ,
  boxHeader: {
    flex: 1, justifyContent: 'center', alignItems: 'center', padding: 20, alignItems: 'center', backgroundColor: '#F7941D'
  },
  imgWrapper: {
    width: 100, height: 100,
    backgroundColor: '#fff',
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#fff', padding: 5
  },
  imgSize: {
    width: "100%", height: "100%",
  },
  boxName: {
    justifyContent: 'center', alignItems: 'center', marginTop: 10
  },
  name: {
    color: 'white', fontWeight: 'bold'
  },
  email: {
    color: 'white', fontSize: 13,
  },
  menuText: {
    color: '#555555',
    fontSize: 13,
    fontWeight: 'bold',
  },
  menuIcon: {
    width: 20,
    height: 20,
    marginRight: 30
  },
  menuWrapper: {
    flex: 3, backgroundColor: '#fff', paddingTop: 10
  },
})