import React, { Component } from 'react';
import { createUser } from '../Actions/KnongDaiAction';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux'
import {
    View, Text,
    StyleSheet, Image,
    TouchableWithoutFeedback,
    StatusBar,
    TextInput,
    SafeAreaView,
    Keyboard, TouchableOpacity,
    KeyboardAvoidingView,
    Dimensions,
    Platform,
    AsyncStorage,
    ImageBackground,
} from 'react-native';

var { height, width } = Dimensions.get('window')

class RegisterScreen extends Component {
    static navigationOptions = {
        header: null,
    }
    constructor(props) {
        super(props);
        this.state = {
            facebookId: this.props.facebookId,
            phoneNumber: this.props.phoneNumber,
            userName: '',
            gender: ''
        };
    }
    async saveUser(facebookId, phoneNumber, userName, gender) {
        //here you store two texts with two different key or concatenate text1 and text2 
        //store the both texts with one key. 
        try {
            await AsyncStorage.setItem('facebookId', facebookId);
            await AsyncStorage.setItem('phoneNumber', phoneNumber);
            await AsyncStorage.setItem('userName', userName);
            await AsyncStorage.setItem('gender', gender);
        } catch (error) {
            console.log("Error saving data" + error);
        }
    }
    getUser = async () => {
        try {
            const facebookId = await AsyncStorage.getItem('facebookId');
            const phoneNumber = await AsyncStorage.getItem('phoneNumber');
            const userName = await AsyncStorage.getItem('userName');
            const gender = await AsyncStorage.getItem('gender');
            // alert(facebookId)
            console.log("phoneNumber===>",phoneNumber)
            console.log("userName===>",userName)
            console.log("facebookID===>",facebookId)
            console.log("Gender===>",gender)
            this.setState({ facebookId, phoneNumber, userName, gender });
           
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }

    handleOnRegister() {
        this.props.createUser(this.state)
        Actions.drawerClose();
        Actions.home();
        this.saveUser(this.state.facebookId,this.state.phoneNumber,this.state.userName,this.state.gender)
        this.getUser()
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="dark-content" />
                <KeyboardAvoidingView  style={styles.container}>
                    <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss} >
                        <View  style={styles.container}>
                            <ImageBackground source={require('../Image/Search.png') }
                                style={{width:'100%',height:'100%'}}
                            >
                                <View style={styles.logoContainer}>
                                    <Image
                                        style={styles.logo}
                                        source={require('../Image/kd-circle.png')}
                                    />
                                    <Text style={styles.title}>ចុះឈ្មោះ</Text>
                                    <Text style={styles.title}>FacebookID : {this.props.facebookId}</Text>
                                    
                                    <Text style={styles.title}>PhoneNumber: {this.props.phoneNumber}</Text>
                                </View>
                                <View style={[styles.infoContainer]}>
                                    <TextInput style={styles.input}
                                        placeholder='UserName '
                                        placeholderTextColor='rgba(255,255,255,0.8)'
                                        keyboardType='default'
                                        returnKeyType='next'
                                        autoCorrect={false}
                                        ref={'userName'}
                                        onChangeText={(userName) => this.setState({ userName })}
                                    />
                                    <TextInput style={styles.input}
                                        placeholder='Gender'
                                        placeholderTextColor='rgba(255,255,255,0.8)'
                                        keyboardType='default'
                                        returnKeyType='go'
                                        autoCorrect={false}
                                        onChangeText={(gender) => this.setState({ gender })}
                                        ref={'gender'}
                                    />
                                    <TouchableOpacity style={styles.buttonContainer}
                                        onPress={() => this.handleOnRegister()}
                                    >
                                        <Text style={styles.buttonText}>REGISTER</Text>
                                    </TouchableOpacity>
                                </View>
                            </ImageBackground>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isCreateUser: state.knongdais.isCreateUser,
    }
}
export default connect(mapStateToProps, { createUser })(RegisterScreen);
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3E7F83',
        flexDirection: 'column',
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
    },
    logo: {
        width:  128,
        height: 128,
    },
    title: {
        color: 'white',
        fontSize: 13,
        textAlign: 'center',
        marginTop: 5,
        opacity: 0.9
    },
    infoContainer: {
        height: 200,
        padding: 20,
        paddingTop:0,
    },
    input: {
        marginVertical: 10,
        height: 40,
        backgroundColor: 'rgba(0,0,0,0.2)',
        paddingHorizontal: 10,
        color: '#fff'
    },
    buttonContainer: {
        backgroundColor: '#F7941D',
        paddingVertical: 10,
        marginVertical: 10,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
    }
})