import React, { Component } from 'react';
import { View, Text,Platform, WebView, NetInfo, StyleSheet, ActivityIndicator } from 'react-native';
import MyWebView from 'react-native-webview-autoheight'

export default class WebViewResult extends Component {
  ActivityIndicatorLoadingView() {
    //making a view to show to while loading the webpage
    return (
      <ActivityIndicator
        color="#009688"
        size="large"
        style={styles.ActivityIndicatorStyle}
      />
    );
  }
  render() {
      return (
        // <MyWebView
        //   source={{ uri: this.props.link }}
        //   startInLoadingState={true}
        // />

        <WebView
          style={styles.container}
          //Loading URL
          source={{ uri: this.props.link }}
          //Enable Javascript support
          javaScriptEnabled={false}
          //For the Cache
          domStorageEnabled={true}
          //View to show while loading the webpage
          renderLoading={this.ActivityIndicatorLoadingView}
          //Want to show the view or not
          startInLoadingState={true}
        />
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop:Platform.OS==='ios' ?20 :0,
  },
  video: {
    marginTop: 20,
    maxHeight: 200,
    width: 320,
    flex: 1
  },
  ActivityIndicatorStyle:{
    flex:1,
    justifyContent:'center',
    alignSelf: 'center',
  }
});
