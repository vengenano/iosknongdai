import React, { Component } from 'react'
import { View, Image, StyleSheet, } from 'react-native'

const ImageLogo = () => (
   <View style={styles.imgBox}>
      <View style={styles.circleBox}>
         <Image 
         source={require('../Image/kd-circle1.png')}
         style={{width:120,height:120}}
         />
      </View>   
    </View>
)
export default ImageLogo

const styles = StyleSheet.create({
   imgBox:{padding:30,flex:1,backgroundColor:'whitesmoke',alignItems:'center'
   }
   ,
   circleBox:{
      backgroundColor:'white',padding:1,borderRadius:120
  },
})