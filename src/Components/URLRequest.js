import React, { Component } from 'react';
import { View, Text,StyleSheet, } from 'react-native';
import {Card} from 'native-base'
import URLRequestInput from './URLRequestInput'
export default class URLRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.containerfluid}>
        <Card>
          <Text style={styles.title}> Enter the website’s info </Text>
         
        </Card> 
        <URLRequestInput />       
      </View>
    );
  }
}

const styles=StyleSheet.create({
    containerfluid:{
        flex:1     
    },
    title:{
        fontSize:20,
        color:'#F7941D',
        fontFamily: 'Roboto',
        alignSelf: 'center',
        marginBottom: 10,
    }
})
